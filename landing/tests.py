from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from .views import *
from .models import *
from .forms import *
from django.utils import timezone
from django.http import HttpRequest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

# Create your tests here.
class landingTest(TestCase):

    def test_landing_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_landing_using_index_template(self):
        response = Client().get('/')
        self.assertTemplateUsed( response, 'index.html')

    def test_landing_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_model_can_create_new_activity(self):
        new_status = Diary.objects.create(date = timezone.now().__str__(),mystatus = 'Saya baik-baik saja')
        counting_object_status = Diary.objects.all().count()
        self.assertEqual(counting_object_status, 1)
    
    def test_landing_return_httpresponse(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Hello, Apa kabar?', html_response)

    def test_can_save_POST_request(self):
        response = self.client.post('/add_activity/', data={'date': '2017-10-12T14:14', 'mystatus' : 'Saya baik-baik saja'})
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/')

class landingFunctionalTest(TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(landingFunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.implicitly_wait(3)
        self.selenium.quit()

    def test_input_todo(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('https://danielanderson.herokuapp.com/')
        # find the form element
        form = selenium.find_element_by_name('mystatus')
        submit = selenium.find_element_by_id('submit_button')

        # Fill the form with data
        form.send_keys('Coba-coba')
        # submitting the form
        submit.send_keys(Keys.RETURN)
        
        self.assertIn('Coba-coba', self.selenium.page_source)
    
    def test_hello_exist(self):
        selenium = self.selenium
        selenium.get('https://danielanderson.herokuapp.com/')
        hello = selenium.find_element_by_id('hello_text')
        self.assertEqual(hello,'Hello, Apa kabar?' )
    
    def test_my_status_exist(self):
        selenium = self.selenium
        selenium.get('https://danielanderson.herokuapp.com/')
        stat = selenium.find_element_by_id('status_text')
        self.assertEqual(stat, 'My Status')

    def test_bg_color_css_exist(self):
        selenium = self.selenium
        selenium.get('https://danielanderson.herokuapp.com/')
        bgcolor = selenium.find_element_by_css_selector('body')
        bgcolor_value = bgcolor.value_of_css_property('background-color')
        self.assertEqual(bgcolor_value, 'rgba(0, 37, 107, 1)')

    def test_font_color_css_exist(self):
        selenium = self.selenium
        selenium.get('https://danielanderson.herokuapp.com/')
        color = selenium.find_element_by_css_selector('body')
        color_value = color.value_of_css_property('color')
        self.assertEqual(color_value, 'rgba(255, 255, 255, 1)')
