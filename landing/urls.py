from django.urls import path
from . import views
from .views import add_activity
#url for app
urlpatterns = [
    path('', views.index, name='index'),
    path('add_activity/',add_activity, name='add_activity'),
]