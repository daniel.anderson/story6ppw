from django import forms

class DiaryForm(forms.Form):
    attribute = {
        'class' : 'form-control'
    }

    mystatus = forms.CharField(label='status', required = True, max_length=300, widget = forms.TextInput(attrs = attribute))