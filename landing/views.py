from django.shortcuts import render
from .models import Diary
from .forms import DiaryForm
from datetime import datetime
from django.http import HttpResponseRedirect

# Create your views here.
response = {}
def index(request):
    response['regiskey'] = DiaryForm
    response['displaykey'] = Diary.objects.all()[::-1]
    return render(request, 'index.html', response)

def add_activity(request):
    diary = DiaryForm(request.POST)
    if (request.method == 'POST'):
        response['mystatus'] = request.POST['mystatus'] if request.POST['mystatus'] != '' else 'kosong'
        diary = Diary(mystatus=response['mystatus'])
        diary.save()
    return HttpResponseRedirect('/')
