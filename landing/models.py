from django.db import models
from django.utils import timezone
from datetime import datetime

# Create your models here.
class Diary(models.Model):
    mystatus = models.CharField(max_length = 300)
    date = models.DateTimeField(default = timezone.now())